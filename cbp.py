#!/usr/bin/python
import cbpro, time
from datetime import datetime, timedelta
from itertools import islice
from colorama import Fore, Back, Style
from influxdb import InfluxDBClient

client = InfluxDBClient("prometheus.xxxx.com", 8086, "username", "password", "crypto")
key="xxxx"
secret="xxxx"
passwd="xxxx"
auth_client = cbpro.AuthenticatedClient(key, secret, passwd)

num = 0
lastprice = 0
while 1:
        data = auth_client.get_product_ticker(product_id='ETH-DAI')
        if lastprice != data['price']:
                num = num + 1
                if lastprice < data['price']:
                        print data['time'], Fore.GREEN, data['price'], Style.RESET_ALL
                else:
                        print data['time'], Fore.RED, data['price'], Style.RESET_ALL
                lastprice = data['price']
                if num > 20:
                        x = list(islice(auth_client.get_orders(), 10))
                        for o in x:
                                print o["created_at"], o["status"], o["product_id"], o["price"], o["size"], o["type"]
                        num = 0
                utcnow = datetime.utcnow()
                json_body = [
                        {
                            "measurement": "eth-dai",
                            "time": utcnow.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
                            "fields": {
                                "price": float(data['price']),
                                "volume": float(data['volume']),
                            }
                        }
                ]
                client.write_points(json_body)
        time.sleep(30)